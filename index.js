function DateInterval(interval_spec) {
	var re = /^P(\d*Y)?(\d*M)?(\d*W)?(\d*D)?(T(\d*H)?(\d*M)?(\d*S)?)?$/;
	if (!re.test(interval_spec))
		throw new Error('Unknown or bad format (' + interval_spec + ')');

	var matches = re.exec(interval_spec);
	this.y = matches[1] ? Number(matches[1].slice(0, -1)) : 0;
	this.m = matches[2] ? Number(matches[2].slice(0, -1)) : 0;
	this.d = matches[3] ? Number(matches[3].slice(0, -1)) * 7 : 0;
	if (matches[4])
		this.d += Number(matches[4].slice(0, -1));
	this.h = matches[6] ? Number(matches[6].slice(0, -1)) : 0;
	this.i = matches[7] ? Number(matches[7].slice(0, -1)) : 0;
	this.s = matches[8] ? Number(matches[8].slice(0, -1)) : 0;
}

DateInterval.prototype.getTime = function() {
	var retVal = this.s;
	retVal += this.i * 60;
	retVal += this.h * 3600;
	retVal += this.d * 24 * 3600;
	retVal += this.m ? ((this.m * 30 * 24) + 10) * 3600 : 0; // 30 days 10 hours
	retVal += (this.y * 365.25 * 30 * 24) * 3600;
	return retVal * 1000;
}

function DatePeriod(/* Date */ start, /* DateInterval */ interval, /* DateTime */ end) {
	var recurrences = 0;
	if (end instanceof Date) {
		recurrences = Math.ceil((end.getTime() - start.getTime()) / interval.getTime());
	} else {
		recurrences = end;
	}
	var retVal = [start],
		current = new Date(start.getTime());
	while(--recurrences) {
		current = new Date(current.getTime() + interval.getTime()); 
		retVal.push(current);
	}
	return retVal;
}

module.exports = {
	DateInterval: DateInterval,
	DatePeriod: DatePeriod
};
